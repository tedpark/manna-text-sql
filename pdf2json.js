// let fs = require("fs"),
//   PDFParser = require("pdf2json");
// let pdfParser = new PDFParser();

// pdfParser.on("pdfParser_dataError", errData =>
//   console.error(errData.parserError)
// );
// pdfParser.on("pdfParser_dataReady", pdfData => {
//   fs.writeFile("/pdf2json/test/F1040EZ.json", JSON.stringify(pdfData));
// });

// fs.readFile(__dirname + "/cmn-cu89s_all.pdf", (err, pdfBuffer) => {
//   if (!err) {
//     pdfParser.parseBuffer(pdfBuffer);
//     console.log(pdfBuffer);
//   }
// });

let fs = require("fs"),
  PDFParser = require("pdf2json");

let pdfParser = new PDFParser();

pdfParser.on("pdfParser_dataError", errData =>
  console.error(errData.parserError)
);
pdfParser.on("pdfParser_dataReady", pdfData => {
  fs.writeFile("sss.json", JSON.stringify(pdfData));
});

pdfParser.loadPDF("cmn-cu89s_all.pdf");
// pdfParser.loadPDF("pdf.pdf");
