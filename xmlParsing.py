# from xml.dom.minidom import parse

# document = parse('cmn-cu89t_osis.xml')

# actors = document.getElementsByTagName("verse")
# print(actors.getAttribute('osisID'))
# for act in actors:
#     name = act.getAttribute('osisID')
#     for node in act.childNodes:
#         if node.nodeType == node.TEXT_NODE:
#             print("{} - {}".format(name, node.data))

# Hamlet - the Prince of Denmark
# Polonius - Ophelias father

import xmltodict

message = """<?xml version="1.0"?><note><to>Tove</to><from>Jani</from><heading>Reminder</heading><body>Don't forget me this weekend!</body></note>"""
# print xmltodict.parse(message)['note']

OrderedDict([(u'to', u'Tove'), (u'from', u'Jani'), (u'heading',
                                                    u'Reminder'), (u'body', u"Don't forget me this weekend!")])
print dict(xmltodict.parse(message)['note'])
