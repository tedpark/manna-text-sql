var striptags = require("striptags");
const fs = require("fs");
var i = 0;

fs.readFile(__dirname + "/cmn-cu89t_osis.xml", function(err, data) {
  //   console.log(JSON.parse(data).osis.osisText.div[0]);
  // console.log(JSON.parse(data).osis.osisText.div[1].div[0].div[12]);
  //   striptags(data);
  //   console.log(data.toString());
  //   console.log(striptags(data.toString(), [], "\n"));
  //   console.log(i);

  fs.appendFile("log1.txt", striptags(data.toString()), function(err) {
    if (err) {
      // append failed
    } else {
      // done
    }
  });
});

var html =
  '<a href="https://example.com">' +
  "lorem ipsum <strong>dolor</strong> <em>sit</em> amet" +
  "</a>";

striptags(html);
striptags(html, "<strong>");
striptags(html, ["a"]);
striptags(html, [], "\n");
